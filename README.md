Temperature Reading Site

To run, open the solution file (.sln) and click the green debug button (or click ctr + f5).

Navigate through the website to see the temperature readings and sensor profile (as well as edit the sensor profile)