﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WovynTemperature.Utils;

namespace WovynTemperature.Controllers
{
	public class ReadingsController : Controller
	{
		// GET: Readings
		public ActionResult Index()
		{
			return View();
		}

		[HttpGet]
		public ActionResult Readings()
		{
			var temperatureReadings = TemperaturePicoInterface.GetTemperatureReadings();
			temperatureReadings.Reverse();
			var temperatureViolationReadings = TemperaturePicoInterface.GetTemperatureViolationReadings();
			TemperaturePicoInterface.MarkTemperatureViolations(temperatureReadings, temperatureViolationReadings);
			return PartialView("_Readings", temperatureReadings);
		}
	}
}