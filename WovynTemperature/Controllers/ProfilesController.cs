﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WovynTemperature.Models;
using WovynTemperature.Utils;

namespace WovynTemperature.Controllers
{
	public class ProfilesController : Controller
	{
		// GET: Profiles
		public ActionResult Index()
		{
			return View(TemperaturePicoInterface.GetTemperatureProfile());
		}

		[HttpGet]
		public ActionResult TemperatureProfile()
		{
			return PartialView("_Profile", TemperaturePicoInterface.GetTemperatureProfile());
		}

		// GET: Profiles/Edit/5
		public ActionResult Edit()
		{
			return View(TemperaturePicoInterface.GetTemperatureProfile());
		}

		// POST: Profiles/Edit/5
		[HttpPost]
		public ActionResult Edit(TemperatureProfile profile)
		{
			try
			{
				TemperaturePicoInterface.SetTemperatureProfile(profile);
				return RedirectToAction("Index");
			}
			catch
			{
				return View(profile);
			}
		}
	}
}
