﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WovynTemperature.Models;

namespace WovynTemperature.Utils
{
	public static class TemperaturePicoInterface
	{
		private static readonly string BASE_EVENT_URI = "http://localhost:8080/sky/event/";
		private static readonly string DOMAIN = "sensor/";
		private static readonly string TYPE = "profile_updated";

		private static readonly string BASE_QUERY_URI = "http://localhost:8080/sky/cloud/";
		private static readonly string TEMPERATURE_RID = "temperature_store/";
		private static readonly string TEMPERATURE_READINGS_FUNCTION = "temperatures";
		private static readonly string TEMPERATURE_VIOLATIONS_FUNCTION = "threshold_violations";

		private static readonly string PROFILE_RID = "sensor_profile/";
		private static readonly string PROFILE_FUNCTION = "profile";

		private static readonly string ECI = "LheVoXquHmvrk3qfXpLVch/";

		public static List<TemperatureReading> GetTemperatureReadings()
		{
			WebRequest request = new WebRequest();
			string response = request.Get(BASE_QUERY_URI + ECI + TEMPERATURE_RID + TEMPERATURE_READINGS_FUNCTION);
			return JsonConvert.DeserializeObject<List<TemperatureReading>>(response);
		}

		public static List<TemperatureReading> GetTemperatureViolationReadings()
		{
			WebRequest request = new WebRequest();
			string response = request.Get(BASE_QUERY_URI + ECI + TEMPERATURE_RID + TEMPERATURE_VIOLATIONS_FUNCTION);
			return JsonConvert.DeserializeObject<List<TemperatureReading>>(response);
		}

		public static void MarkTemperatureViolations(List<TemperatureReading> temperatureReadings, List<TemperatureReading> temperatureViolations)
		{
			foreach (TemperatureReading reading in temperatureReadings)
			{
				foreach (TemperatureReading violation in temperatureViolations)
				{
					if (reading.TimeStamp.Equals(violation.TimeStamp))
					{
						reading.IsTemperatureViolation = true;
						break;
					}
				}
			}
		}

		public static TemperatureProfile GetTemperatureProfile()
		{
			WebRequest request = new WebRequest();
			string response = request.Get(BASE_QUERY_URI + ECI + PROFILE_RID + PROFILE_FUNCTION);
			return JsonConvert.DeserializeObject<TemperatureProfile>(response);
		}

		public static void SetTemperatureProfile(TemperatureProfile temperatureProfile)
		{
			WebRequest request = new WebRequest();
			var requestString = JsonConvert.SerializeObject(temperatureProfile);
			string response = request.Post(BASE_EVENT_URI + ECI + GenerateURIUUID() + DOMAIN + TYPE, requestString, "application/json");
		}

		private static string GenerateURIUUID()
		{
			return Guid.NewGuid().ToString() + "/";
		}
	}
}